#!/usr/bin/python
# -*- coding: utf-8 -*-

#
# Interface for different mail protocols
#
# @author Dmytryi Slupytskyi <azmelanar@gmail.com>
#

class Protocols(object):
	def __init__(self, host, port, secure = 0, timeout = 7):
		self.host = str(host).lower()
		self.port = int(port)
		self.secure = int(secure)
		self.timeout = int(timeout)

	def login(self, auth_type, username, password):
		self.auth_type = str(auth_type).lower()
		self.username = str(username)
		self.password = str(password)

	def close(self):
		raise NotImplementedError
