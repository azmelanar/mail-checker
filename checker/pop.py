#!/usr/bin/python
# -*- coding: utf-8 -*-

#
# Library for POP connection
#
# @author Dmytryi Slupytskyi <azmelanar@gmail.com>
#

import poplib
from protocols import Protocols
from timeout import Timeout

class POP(Protocols):

	class ConnectError(Exception): pass
	class TimeoutError(Exception): pass
	class MechanismsError(Exception): pass
	class CredentialsError(Exception): pass

	def __init__(self, host = '', port = 143, secure = 0, timeout = 7):
		super(self.__class__, self).__init__(host, port, secure, timeout)
		self.__open()
	
	def __open(self):
		try:
			with Timeout(self.timeout):
				if self.secure == False :
					self.pop = poplib.POP3(self.host, self.port)
				else:
					self.pop = poplib.POP3_SSL(self.host, self.port)
				self.pop.set_debuglevel(4)
		except Timeout.TimeoutError:
			raise self.TimeoutError("Time out!")
		except Exception:
			raise self.ConnectError ("Cannot establish connect with remote host!")

	def welcome(self):
		return self.pop.welcome

	def login(self, auth_type, username, password):
		super(self.__class__, self).login(auth_type, username, password)
		mechanisms = {
			'login' : self.__login,
			'apop' : self.__apop,
		}

		try:
			return mechanisms[self.auth_type]()
		except KeyError:
			raise self.MechanismsError("Not supported mechanism authentication!")
		except Exception:
			raise self.CredentialsError("Incorrect username or password!")

	def __login(self):
		self.pop.user(self.username)
		return self.pop.pass_(self.password)

	def __apop(self):
		return self.pop.apop(self.username, self.password)

	def close(self):
		self.pop.quit()
