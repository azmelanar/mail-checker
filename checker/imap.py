#!/usr/bin/python
# -*- coding: utf-8 -*-

#
# Library for IMAP connection
#
# @author Dmytryi Slupytskyi <azmelanar@gmail.com>
#

import imaplib
from protocols import Protocols
from timeout import Timeout

imaplib.Debug = 4

class IMAP(Protocols):

	class ConnectError(Exception): pass
	class TimeoutError(Exception): pass
	class MechanismsError(Exception): pass
	class CredentialsError(Exception): pass

	def __init__(self, host = '', port = 143, secure = 0, timeout = 7):
		super(self.__class__, self).__init__(host, port, secure, timeout)
		self.__open()
	
	def __open(self):
		try:
			with Timeout(self.timeout):
				if self.secure == False :
					self.imap = imaplib.IMAP4(self.host, self.port)
				else:
					self.imap = imaplib.IMAP4_SSL(self.host, self.port)
		except Timeout.TimeoutError:
			raise self.TimeoutError("Time out!")
		except Exception:
			raise self.ConnectError ("Cannot establish connect with remote host!")

	def welcome(self):
		return self.imap.welcome

	def __message(self, msg):
		code = msg[0]
		data = msg[1][0]
		return "* " + code + " " + data

	def login(self, auth_type, username, password):
		super(self.__class__, self).login(auth_type, username, password)
		mechanisms = {
			'login' : self.__login,
			'cram_md5' : self.__cram_md5,
		}

		try:
			return self.__message(mechanisms[self.auth_type]())
		except KeyError:
			raise self.MechanismsError("Not supported mechanism authentication!")
		except Exception:
			raise self.CredentialsError("Incorrect username or password!")

	def __login(self):
		return self.imap.login(self.username, self.password)

	def __cram_md5(self):
		return self.imap.login_cram_md5(self.username, self.password)

	def close(self):
		self.imap.logout()
