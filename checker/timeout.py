#!/usr/bin/python
# -*- coding: utf-8 -*-

import signal
import time

class Timeout():
	
	class TimeoutError(Exception): pass

	def __init__(self, sec):
		self.sec = sec

	def __enter__(self):
		signal.signal(signal.SIGALRM, self.exception)
		signal.alarm(self.sec)

	def __exit__(self, *args):
		signal.alarm(0)

	def exception(self, *args):
		raise self.TimeoutError('Timed out!')