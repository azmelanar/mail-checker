#!/usr/bin/python
# -*- coding: utf-8 -*-

#
# Library for SMTP connection
#
# @author Dmytryi Slupytskyi <azmelanar@gmail.com>
#

import smtplib
from protocols import Protocols
from timeout import Timeout

class SMTP_(Protocols):

	class ConnectError(Exception): pass
	class TimeoutError(Exception): pass
	class MechanismsError(Exception): pass
	class CredentialsError(Exception): pass

	def __init__(self, host = '', port = 143, secure = 0, timeout = 7):
		super(self.__class__, self).__init__(host, port, secure, timeout)
		self.__open()
	
	def __open(self):
		try:
			with Timeout(self.timeout):
				if self.secure == False :
					self.smtp = smtplib.SMTP(self.host, self.port)
				else:
					self.smtp = smtplib.SMTP_SSL(self.host, self.port)
				self.smtp.set_debuglevel(4)
		except Timeout.TimeoutError:
			raise self.TimeoutError("Time out!")
		except Exception:
			raise self.ConnectError ("Cannot establish connect with remote host!")

	def welcome(self):
		return self.__message(self.smtp.helo())

	def __message(self, msg):
		code = str(msg[0])
		data = msg[1]
		return code + " " + data

	def login(self, auth_type, username, password):
		super(self.__class__, self).login(auth_type, username, password)
		mechanisms = {
			'login' : self.__login,
		}

		try:
			return self.__message(mechanisms[self.auth_type]())
		except KeyError:
			raise self.MechanismsError("Not supported mechanism authentication!")
		except Exception:
			raise self.CredentialsError("Incorrect username or password!")

	def __login(self):
		return self.smtp.login(self.username, self.password)

	def close(self):
		self.smtp.quit()
