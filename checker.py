#!/usr/bin/python
# -*- coding: utf-8 -*-

#
# Command-line application for checking connection 
# and optional authentication to remote host via IMAP and POP
#
# @author Dmytryi Slupytskyi <azmelanar@gmail.com>
#

import os
import sys
import argparse
from checker.imap import IMAP
from checker.pop import POP

__version__ = "0.1"

info = """
Information about supported mechanisms authentication:

- for IMAP:
    login
    cram_md5

- for POP:
    login
    apop
"""

parser = argparse.ArgumentParser(
	prog="checker.py", 
	description="Checker connection to remote host and optional authentication via IMAP or POP.", 
	formatter_class=argparse.RawDescriptionHelpFormatter,
	epilog=info, 
	add_help=False
)

parser.add_argument("protocol", choices=["imap","pop"], help="set protocol")
parser.add_argument("-h", "--host", help="set remote host")
parser.add_argument("-p", "--port", type=int, required=True, help="set port for remote host")
parser.add_argument("-s", "--secure", action="store_true", help="make secure connect to remote host")
parser.add_argument("-t", "--timeout", type=int, default=7, help="set timeout for initialization connect")
parser.add_argument("-a", "--auth", default="login", help="set authentication method according to protocol")
parser.add_argument("-c", "--credentials", nargs=2, help="set credentials for checking authentication", metavar=("ACCOUNT","PASSWORD"))
parser.add_argument("-H", "--help", action="help", help="show this help message and exit")
parser.add_argument('-V', '--version', action="version", version="%(prog)s version " + __version__)

args = parser.parse_args()

def imap(host, port, secure, timeout):
	return IMAP(host, port, secure, timeout)

def pop(host, port, secure, timeout):
	return POP(host, port, secure, timeout)

def connection(protocol, host, port, secure, timeout):
	protocols = {
		'IMAP' : imap,
		'POP' : pop,
	}
	return protocols[protocol](host, port, secure, timeout)

def main():

	try:
		protocol = args.protocol.upper()
		obj = connection(protocol, args.host, args.port, args.secure, args.timeout)
		if protocol == 'POP':
			print "*resp* " + obj.welcome()

		if args.credentials:
			username = args.credentials[0]
			password = args.credentials[1]
			obj.login(args.auth, username, password)

		obj.close()
	except Exception as e:
		print "error: " + e.message

if __name__ == '__main__':
	main()
