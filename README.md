# Mail Checker

Command-line application for checking connection and optional authentication to remote host via IMAP or POP.

## Usage

       usage: checker.py [-h HOST] -p PORT [-s] [-t TIMEOUT] [-a AUTH]

                   [-c ACCOUNT PASSWORD] [-H] [-V]

                   {imap,pop}
 
       Checker connection to remote host and optional authentication via IMAP or POP.
 
       positional arguments:

       {imap,pop}            set protocol
 
       optional arguments:

       -h HOST, --host HOST  set remote host

       -p PORT, --port PORT  set port for remote host

       -s, --secure          make secure connect to remote host

       -t TIMEOUT, --timeout TIMEOUT

                         set timeout for initialization connect

       -a AUTH, --auth AUTH  set authentication method according to protocol

       -c ACCOUNT PASSWORD, --credentials ACCOUNT PASSWORD

                         set credentials for checking authentication

       -H, --help            show this help message and exit

       -V, --version         show program's version number and exit
 
       Information about supported mechanisms authentication:
 
       - for IMAP:

           *  login

           *  cram_md5
 
       - for POP:

           *  login

           *  apop